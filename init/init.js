
import db from '../src/db'
import {default as gps} from "./data/gps.json";

console.log("Adding Extension")

const addExtension = () => {
    db.sequelize.query(`
        CREATE EXTENSION IF NOT EXISTS pg_trgm;
        CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
    `)
    .then(() => {
        console.log('Adding Extension Done');
    })
    .catch(err => {
        console.log("err: ", err)
    })
}


console.log('Attention : db schema recreate started...');
db.sequelize.sync({ force: true, logging: console.log })
    .then(() => {
        return addExtension();
    })
    .then(() => {
        return runBasicETL();
    })
    .catch(err => {
        console.log("err: ", err)
    })


const runBasicETL = async () => {
    
    gps.forEach(async value => {
       await db.gps.upsert(value);
    });  


}

