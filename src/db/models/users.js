module.exports = function(db, DataTypes) {
    const tableName = 'users';
    const newSchema = {
      id: { type: DataTypes.UUID  , primaryKey: true, defaultValue: db.fn('uuid_generate_v4') },
      name: { type: DataTypes.STRING(255), allowNull: false, required: true, },
      user_id: { type: DataTypes.STRING(255), allowNull: false, required: true, },
      password: { type: DataTypes.STRING(255), allowNull: false, required: true, },
      email: { type: DataTypes.STRING(255), allowNull: false, required: true, unique:true },
      login_token: { type: DataTypes.UUID, defaultValue: db.fn('uuid_generate_v4'), unique:true }
    }
  
    const defaultfields = {
      attributes: { exclude: ['password'] },
      timestamps: false,
      indexes: [
            {
              name: 'unique_email',
              unique: true,
              fields: [db.fn('lower', db.col('email'))]
            }
      ]
    }
  
    const model = db.define(tableName, newSchema, defaultfields);  
    return model;
  };