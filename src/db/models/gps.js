module.exports = function(db, DataTypes) {
    const tableName = 'gps';
    const newSchema = {
      id: { type: DataTypes.UUID  , primaryKey: true, defaultValue: db.fn('uuid_generate_v4') },
      device_id: { type: DataTypes.STRING(255), allowNull: false, required: true, },
      device_type: { type: DataTypes.STRING(255), allowNull: false, required: true, },
      timestamp: { type: DataTypes.DATE, allowNull: false, required: true, },
      location: { type: DataTypes.STRING(255), allowNull: false, required: true },
    }
  
    const defaultfields = {
      timestamps: false,
      indexes: [
            {
              fields: ["device_id"]
            },
            {
              fields: ["device_type"]
            }            
      ]
    }
  
    const model = db.define(tableName, newSchema, defaultfields);  
    return model;
  };