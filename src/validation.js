import * as Yup from 'yup';

const REGEX_EMAIL = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const LoginValidation = Yup.object().shape({
    password: Yup.string()
        .min(6, 'Password minimal 6 - 30 karakter')
        .max(30, 'Password terlalu panjang')
        .required('Required'),
    // username: Yup.string().trim().required('Required').matches(REGEX_EMAIL, "Alamat email tidak valid") << disabled : accept both email and user_id
    username : Yup.string().trim().required('Required').min(3, "Username terlalu pendek").max(100, 'Username terlalu panjang'),
});

export const SignupValidation = Yup.object().shape({
    user_id : Yup.string().trim().required('Required').min(3, "Username terlalu singkat").max(100, 'Username terlalu panjang'),
    password: Yup.string()
        .min(6, 'Password minimal 6 - 30 karakter')
        .max(30, 'Password terlalu panjang')
        .required('Required'),
    email: Yup.string().trim().required('Required').matches(REGEX_EMAIL, "Alamat email tidak valid"),
    name : Yup.string().trim().required('Required').min(3, "Masukan nama lengkap sesuai KTP").max(100, 'Nama terlalu panjang'),
});