import async from "async";
import  { SignupValidation, LoginValidation } from "../validation";
import pg from "../db";
import sequelize, { Op } from "sequelize";

export function validateToken(token) {

  function findToken(callback){
      pg.users
          .findOne(
            {
              attributes:['id'],
              where : {
                login_token : token
              },
              raw:true
            }
          )
          .then(res => {
            callback(null, true)
          })
          .catch(err => {
              callback(err, null);
          })                
  }

  return new Promise((resolve, reject) => {
      async.auto(
        {
          devices: function (callback) {
            findToken(callback);
          },    
      },
        function (err, results) {
          if(err){
              var _res = {
                status : 400,
                error  : err
              }
              reject(_res)
            }else{
              var _res = {
                status : 200,
                data : results?.devices ?? []
              }
              resolve(_res);
            }      
        }
      );
    });

}


export function createUser(data) {

    function openTransaction(callback) {
        pg.sequelize
          .transaction({
            autocommit: false,
          })
          .then(res => {
            callback(null, res)
        }).catch(err => {
            callback(err, null)
        })    
      }

    function validateUserInput(callback){
        SignupValidation.validate(data)
        .then(res => {
            callback(null, true)
        }).catch(err => {
            callback(err, null)
        })    
    }

    function validateEmail(transaction, callback){
        // Findone is better than count, so we just findOne and count the result
        var _filter = {
            attributes:['email'],
            where: sequelize.where(sequelize.fn('lower', sequelize.col('email')), data.email.toLowerCase())
          }
          pg.users
          .findOne(_filter, {transaction : transaction})
          .then(res => {
            if(res){
                callback("Email sudah terdaftar, silahkan gunakan email lain", false);
            }else{
                callback(null, true)
            }
          })
          .catch(err => {
            callback(err, null);
          })                
    }

    function validateUsername(transaction, callback){
        // Findone is better than count, so we just findOne and count the result
        var _filter = {
            attributes:['user_id'],
            where: sequelize.where(sequelize.fn('lower', sequelize.col('user_id')), data.user_id.toLowerCase())
          }
          pg.users
          .findOne(_filter, {transaction : transaction})
          .then(res => {
            if(res){
                callback("user_id sudah terdaftar, silahkan gunakan user_id lain", false);
            }else{
                callback(null, true)
            }
          })
          .catch(err => {
            callback(err, null);
          })                
    }

    function InsertUser(transaction, callback){
        pg.users
            .create({
                ...data,
                ...{login_token : sequelize.fn("uuid_generate_v4")}
            }, {transaction : transaction})
            .then(res => {
                if(res) callback(null, res);
                else callback("Gagal Registrasi", null);
            })
            .catch(err => {
                callback(err, null);
            })                
    }

    return new Promise((resolve, reject) => {
        async.auto(
          {
            validate: function (callback) {
                validateUserInput(callback);
            },
            transaction: ['validate', function (results, callback) {
                openTransaction(callback);
            }],          
            unique_email: ['transaction', function (results, callback) {
                validateEmail(results.transaction, callback);
            }],
            unique_username: ['transaction', function (results, callback) {
                validateUsername(results.transaction, callback);
            }],            
            create_user: ['unique_username', 'unique_email', function (results, callback) {
                InsertUser(results.transaction, callback);
            }],            
        },
          function (err, results) {
            if(err){
                var _res = {
                  status : 400,
                  error  : err
                }
                results.transaction?.rollback();
                reject(_res)
              }else{
                var _res = {
                  status : 200,
                  data : results?.create_user
                }
                results.transaction?.commit();
                resolve(_res);
              }      
          }
        );
      });

}


export function loginUser(data) {
    
    function openTransaction(callback) {
        pg.sequelize
          .transaction({
            autocommit: false,
          })
          .then(res => {
            callback(null, res)
        }).catch(err => {
            callback(err, null)
        })    
      }

    function validateUserInput(callback){
        LoginValidation.validate(data)
        .then(res => {
            callback(null, true)
        }).catch(err => {
            callback(err, null)
        })    
    }

    function validateUsernameAndPassword(transaction, callback){
        // Since we dont have any encryption on password, just search where email / user_id & password
        var _filter = {
            raw:true,
            attributes:{exclude: ['password']},
            where: {
              [Op.or]: [
                sequelize.where(sequelize.fn('lower', sequelize.col('user_id')), data.username.toLowerCase()),
                sequelize.where(sequelize.fn('lower', sequelize.col('email')), data.username.toLowerCase())
              ],
              password : data.password
            }
          }
          pg.users
          .findOne(_filter, {transaction : transaction})
          .then(res => {

            if(res){
                revalidateToken(res, transaction, callback)
            }else{
                callback("Login gagal - invalid credentials", null)
            }
          })
          .catch(err => {
            callback(err, null);
          })                
    }

    function revalidateToken(data, transaction, callback){
        pg.users
        .update({
            login_token : sequelize.fn("uuid_generate_v4")
        }, {
            returning: ['*'],
            raw:true,
            where: data,            
            transaction : transaction
        })
        .then(res => {
            callback(null, res[1][0]);
        })
        .catch(err => {
            callback(err, null);
        })   
    }

    return new Promise((resolve, reject) => {
        async.auto(
          {
            validate: function (callback) {
                validateUserInput(callback);
            },
            transaction: ['validate', function (results, callback) {
                openTransaction(callback);
            }],          
            auth: ['transaction', function (results, callback) {
                validateUsernameAndPassword(results.transaction, callback);
            }]
        },
          function (err, results) {
            if(err){
                var _res = {
                  status : 400,
                  error  : err
                }
                results.transaction?.rollback();
                reject(_res)
              }else{
                var _res = {
                  status : 200,
                  data : results?.auth
                }
                results.transaction?.commit();
                resolve(_res);
              }      
          }
        );
    });

}