import async from "async";
import pg from "../db";
import sequelize from "sequelize";

export function getAllDevices() {

    function getAllDevices(callback){
        pg.gps
            .findAll(
              {
                attributes:[
                  'device_id', 
                  'device_type',
                  [sequelize.fn('MAX', sequelize.col('timestamp')), "last_seen"]
                ],
                group: ['device_id', 'device_type'],
                raw:true,
                order: [['last_seen', 'DESC']]
              }
            )
            .then(res => {
              callback(null, res)
            })
            .catch(err => {
                callback(err, null);
            })                
    }

    return new Promise((resolve, reject) => {
        async.auto(
          {
            devices: function (callback) {
              getAllDevices(callback);
            },    
        },
          function (err, results) {
            if(err){
                var _res = {
                  status : 400,
                  error  : err
                }
                reject(_res)
              }else{
                var _res = {
                  status : 200,
                  data : results?.devices ?? []
                }
                resolve(_res);
              }      
          }
        );
      });

}


export function findOne(device_id) {

  function find(callback){
      pg.gps
          .findAll(
            {
              raw:true,
              where : {
                device_id : device_id
              },
              order: [['timestamp', 'DESC']]
            }
          )
          .then(res => {
            callback(null, res)
          })
          .catch(err => {
              callback(err, null);
          })                
  }

  return new Promise((resolve, reject) => {
      async.auto(
        {
          data: function (callback) {
            find(callback);
          },    
      },
        function (err, results) {
          if(err){
              var _res = {
                status : 400,
                error  : err
              }
              reject(_res)
            }else{
              var _res = {
                status : 200,
                data : results?.data ?? []
              }
              resolve(_res);
            }      
        }
      );
    });

}
