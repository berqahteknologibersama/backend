import express from "express";
import bodyparser from "express";
import cors from "cors";
import  { sequelize } from "./db/index"; // di import biar langsung konek ke DB()
import  { createUser, loginUser, validateToken } from "./controllers/users";
import  { getAllDevices, findOne } from "./controllers/devices";

const app = express();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
app.use(cors())


const checkLoginToken = async (req, res, next) => {
    if (!req.headers.authorization) {
        return res.status(403).json({ error: 'No credentials sent!' });
    }
    validateToken(req.headers.authorization)
    .then(_res => {
        next();
    }).catch(() => {
        return res.status(401).json({ error: 'Invalid credentials sent!' });
    })    
}

app.get('/', (req, res) => {
  res.send('Hello World');
});

app.post('/signup', (req, res) => {
    createUser(req.body)
    .then(_res => {
        res.json(_res);
    }).catch(err => {
        res.status(err.status).json(err)
    })    
});

app.post('/login', (req, res) => {
    loginUser(req.body)
    .then(_res => {
        res.json(_res);
    }).catch(err => {
        res.status(err.status).json(err)
    })    
});

app.get('/gps', checkLoginToken, (req, res) => {
    getAllDevices()
    .then(_res => {
        res.json(_res);
    }).catch(err => {
        res.status(err.status).json(err)
    })    
});

app.get('/gps/:device_id', checkLoginToken, (req, res) => {
    findOne(req.params.device_id)
    .then(_res => {
        res.json(_res);
    }).catch(err => {
        res.status(err.status).json(err)
    })    
});

app.listen(process.env.PORT, () => {
    console.log(`Listening on port ${process.env.PORT}`)
})
